# encoding: utf-8
"""
Script to run benchmarks defined in an XML parameter file using a model defined
in a python module that provides the following functions:

  record(measureable, brain_region)   - where measureable is "spikes" or
                                        "membrane potential" and brain_region is
                                        a dictionary specifying the brain areas,
                                        layers and cell types to record from.
  set_stimulus(filename, scale_factor) - where filename is a stimulus file in zip
                                        or hdf5 format
  run()

Usage example: python visionbenchmark.py EnRo66_Fig1_X.xml ../Retina/wohrer_Xcell.py

$Id$
"""

import gnosis.xml.objectify as gxo
import sys
import os.path
import numpy
from copy import deepcopy
##from NeuroTools.facets import fkbtools
from Benchmarks import plotting, analysis
import shelve
import urllib
import urllib2
import webbrowser
from getpass import getpass

red     = 0010; green  = 0020; yellow = 0030; blue = 0040;
magenta = 0050; cyan   = 0060; bright = 0100
try:
    import ll.ansistyle
    def colour(col,text):
        return unicode(ll.ansistyle.Text(col, unicode(text)))
except ImportError:
    def colour(col,text):
            return text


CACHE_RESULTS = True

ANALYSES = {
    'histogram': (analysis.histogram, "binwidth", "minimum", "maximum"),
    'spike histogram': (analysis.spike_histogram, "binwidth", "duration"),
    'mean firing rate': (analysis.mean_firing_rate, "duration"),
    'tuning curves': (analysis.tuning_curves, "method"),
    'tuning curve': (analysis.tuning_curves, "method"),
    'curve fitting': (analysis.curve_fitting, "method", "curvetype", "normalization"),
    'peak width': (analysis.peak_widths,),
    'filter-by-preferred': (analysis.filter_by_preferred,),
    'merge trials': (analysis.merge_trials,),
    'f1:f0 ratio': (analysis.f1f0_ratio, "binwidth", "f_stim"),
    'extract parameter': (analysis.extract_parameter, "name"),
}

DIFFERENCE_MEASURES = {'root-mean-square' : analysis.rms_error,
                       'absolute difference' : lambda x1,x2: abs(x1-x2),
                       'simple difference' : lambda x1,x2: x1-x2,
                       'chi-square' : analysis.chi_square,
                       u'χ²': analysis.chi_square,
}


# Global variables (because now it is used also in the function 'register')
difference = None


def print_summary(bp):
    """Print a pretty summary of the benchmark XML file."""
    for parameter in ['id','description','recording.measureable','difference_measure.type']:
        parent = bp
        for node in parameter.split('.'):
            node = getattr(parent, node)
            parent = node
        if isinstance(node,unicode):
            value = node
        else:
            value = node.PCDATA
	##Just change the title dor this parameter
	if parameter == 'difference_measure.type':
	    parameter = 'difference_measure'
        print "%-30s %s" % (colour(bright+red,parameter.title()), colour(yellow,value))

    analyses = bp.analysis
    if not isinstance(analyses, list):
        analyses = [analyses]
    for i, analysis in enumerate(analyses):
        print "%-30s %s" % (colour(bright+red,"Analysis %d" % (i+1,)), colour(yellow,analysis.type))
        try:
            parameters = analysis.parameter
        except AttributeError:
            parameters = []
        if not isinstance(parameters, list):
            parameters = [parameters]
        for p in parameters:
            if hasattr(p, 'units'):
#                print "%s%-20s %s %s" % (" "*25, colour(cyan,p.name+":"), p.value, p.units)
                print "          %s%-25s %s %s" % ("", colour(cyan,p.name+":"), p.value, p.units)
            else:
#                print "%s%-20s %s" % (" "*25, colour(cyan,p.name+":"), p.value)
                print "          %s%-25s %s" % ("", colour(cyan,p.name+":"), p.value)

    print
    for pcol in bp.protocol:
        print colour(bright+red,'Protocol "%s":' % pcol.id)
        fmt = "          %-25s %s %s"
        print fmt % (colour(cyan,"repetitions:"), pcol.repetitions, '')
        print fmt % (colour(cyan,"weight:"), pcol.weight, '')
        print fmt % (colour(cyan,"duration:"), pcol.duration, 'ms')
        print fmt % (colour(cyan,"scale-factor:"), getattr(pcol.stimulus,"scale-factor"), 'pixels/degree')
        print fmt % (colour(cyan,"stimulus:"), pcol.stimulus.img, '')
        print fmt % (colour(cyan,"max-luminance:"), getattr(pcol.stimulus,"max-luminance"), u'cd/m²')
        if hasattr(pcol.comparison_data, 'url'):
            print fmt % (colour(cyan,"comparison-data:"), pcol.comparison_data.url, '')
        elif hasattr(pcol.comparison_data, 'value'):
            print fmt % (colour(cyan,"comparison-data:"), pcol.comparison_data.value, pcol.comparison_data.units)

def brain_region(region_obj):
    """
    Converts the brain_region object from the XML file to a dictionary.
    """
    region_dict = {}
    layers = region_obj.layer
    if not isinstance(layers, list):
        layers = [layers]
    for layer in layers:
        region_dict[layer.name] = {}
        cell_types = layer.cell_type
        if not isinstance(cell_types, list):
            cell_types = [cell_types]
        for cell_type in cell_types:
            region_dict[layer.name][cell_type.name] = int(cell_type.number)
    return region_dict

def filter_variables(stimulus_variables, all_vars, use_vars):
    filtered_variables = deepcopy(stimulus_variables)
    mask = []
    all_vars = [v.strip() for v in all_vars.split(',')]
    use_vars = [v.strip() for v in use_vars.split(',')]
    for var in use_vars:
        mask.append(all_vars.index(var))
    for i,line in enumerate(filtered_variables):
        entries = line.split(',')
        if len(entries) > 1:
            filtered_variables[i] = ",".join(numpy.array(line.split(','))[mask])
    return filtered_variables

def depends_on_input_variable(analysis):
    var = False
    if hasattr(analysis, "parameter"):
        for p in analysis.parameter:
            if p.name == "variable":
                var = p.value
                break
    return var
    

def run_analysis(result_dict, protocol, analysis, stimulus_variables):
    """
    Run an analysis, based on the analysis type and parameters contained in the
    XML parameter file.
    """
    # The analyses dict is indexed by the analysis type, and contains tuples.
    #   Example: (nstats.histc, "numpy.arange(0,duration,binwidth)")
    analysis_info = ANALYSES[analysis.type]
    # The first element of the tuple is the function used for analysis.
    analysis_function = analysis_info[0]
    # There may be data for more than one cell population, in which case the
    # analysis is performed for each set of results
    analysis_results = {}
    for population, result in result_dict.items():
        # The first argument of the analysis function must be the results array obtained
        # from running the model (spike times or membrane potential).
        #   Example: the first argument of nstats.histc is an array of event times
        analysis_parameters = [result,]
        # If the analysis relies on a variable contained in the stimulus file,
        # this is added as the second argument
        dependent_variable = depends_on_input_variable(analysis)
        if dependent_variable:
            filtered_variables = filter_variables(stimulus_variables, protocol.stimulus.variables, dependent_variable) # the stimulus variables may contain multiple variables and we may not want to use all of them
            analysis_parameters.append(filtered_variables)
        # Subsequent elements of the tuple are used to calculate the remaining
        # arguments of the analysis function, in order.
        #   Example: the second argument of nstats.histc is an array of bin boundaries
        # These elements are strings which should evaluate to give the required
        # XML parameter file.
        #   Example: "numpy.arange(0,duration,binwidth)" evaluates to give the bins
        #            array, but requires the variables duration and binwidth to
        #            exist.
        # The duration parameter is always available, and is a parameter of the
        # protocol. Other parameters are set at the level of the benchmark.
        duration = float(protocol.duration)
        if hasattr(analysis, "parameter"):
            for p in analysis.parameter:
                try:
                    exec("%s = %g" % (p.name, float(p.value)))
                except ValueError:
                    exec("%s = '%s'" % (p.name, p.value))
        #     (Note that the XML file also contains units information, but this is
        #      not currently used.)
        # Once all the parameters have been set, the function argument strings are
        # evaluated.
        for p in analysis_info[1:]:
                analysis_parameters.append(eval(p))
        # And then we call the analysis function and return the result.
##        print "\n\nanalysis parameters:", analysis_parameters
        analysis_result = analysis_function(*analysis_parameters)
        analysis_results[population] = analysis_result
    return analysis_results

def compare(model_results, comparison_data, difference_measure, fig=None):
    """Compare model results to experimental results."""
    # Haven't yet thought about the problem of different comparison data for
    # different populations.
    try:
        error_function = DIFFERENCE_MEASURES[difference_measure]
    except KeyError:
        print type(difference_measure)
        print difference_measure
        for k in DIFFERENCE_MEASURES.keys():
            print type(k), k
        raise
    assert isinstance(model_results, dict)
    total_error = 0
    
    if isinstance(comparison_data, basestring): # data from a file
        expt_result_url = comparison_data
        # check it is really a url
        # Load experimental data from file
##        data = fkbtools.getData(expt_result_url)
        data = numpy.loadtxt(urllib2.urlopen(expt_result_url))
        xdata = data[:,0].astype('float')
        ydata = data[:,1].astype('float')
        for population, model_result in model_results.items():
            # Plot both sets of data
            if fig:
                panel = fig.next_panel()
                panel.plot1(xdata,ydata,label="Experimental data")
                assert model_result[0].shape == model_result[1].shape, "x and y data different size: %s != %s" % (model_result[0].shape, model_result[1].shape)
                panel.plot1(model_result[1],model_result[0],label="Simulation results")
                panel.legend(loc='best')
            else:
                panel = None
            total_error += error_function(xdata,ydata,model_result[1],model_result[0],panel)
    else: # a single value
        for population, model_result in model_results.items():
            total_error += error_function(comparison_data[0], model_result[0])
    return total_error

def run_benchmark(parameterfile, modelscript):
    """
    Read parameters from "parameterfile" (XML) and run the model contained
    in the "modelscript" module, which can either be a directly importable
    module name or the path to a python file.
    This module should define three functions: record(), set_stimulus() and run().
    """

    ##define difference as global to use it in the function 'register'
    global difference     

    # Parse the parameter file
    benchmark_parameters = gxo.make_instance(parameterfile)

    # Import the modelscript module
    modelscript = modelscript.rstrip(".py")
    try:                             # try direct import
        exec("import %s" % modelscript)
        mmodule = modelscript
    except (SyntaxError, ImportError): # if that doesn't work, try adding the
                                     # path to the file to sys.path
        path,mmodule = os.path.split(os.path.abspath(modelscript))
        sys.path.append(path)
        try:
            exec("import %s" % mmodule)
        except ImportError:          # can't find model file.
            print colour(red,"Unable to import %s" % modelscript)
            print path, mmodule
            sys.exit(2)
        #sys.path.pop() # do we need this?

    # Give some feedback to the user
    print_summary(benchmark_parameters)

    # Loop over protocols...
    brain_region_dict = brain_region(benchmark_parameters.recording.location.brain_region)
    exec('%s.record("%s",brain_region_dict)' % (mmodule, # tell the model what to record
                                   benchmark_parameters.recording.measureable.PCDATA,))
    if hasattr(benchmark_parameters.protocol[0].comparison_data, "value"):
        single_plot = True
    elif hasattr(benchmark_parameters.protocol[0].comparison_data, "url"):
        single_plot = False
    else:
        raise Exception("Invalid <comparison-data> tag")
    graph_params = {}
    if hasattr(benchmark_parameters, 'graph'):
        graph_params = {
            'xlabel':  benchmark_parameters.graph.xlabel,
            'ylabel':  benchmark_parameters.graph.ylabel,
            'scaling': (benchmark_parameters.graph.xscale, benchmark_parameters.graph.yscale)
        }
    fig = plotting.SimpleMultiplot(single_plot and 1 or len(benchmark_parameters.protocol),
                                   1,
                                   title="Benchmark: %s\nModel: %s.py" % (parameterfile,modelscript),
                                   **graph_params
                                   )
    if single_plot:
        xdata = []; ydata_result = []; ydata_comparison = []
        
    for pcol in benchmark_parameters.protocol:
        print colour(bright+red,'\nRunning protocol "%s":' % pcol.id)
        # Run the model
        exec('stimulus_variables = %s.set_stimulus("%s",scale_factor=%g, max_luminance=%g, background_luminance=%g)' % (mmodule,
                                           pcol.stimulus.img,
                                           float(getattr(pcol.stimulus,"scale-factor")),
                                           float(getattr(pcol.stimulus,"max-luminance")),
                                           float(getattr(pcol.stimulus,"background-luminance")))
            )
        simulation_results = {}
        print colour(yellow,"\nRunning model"),
        for i in range(int(pcol.repetitions)):
            print ".",
            sys.stdout.flush() 
            exec("results = %s.run()" % mmodule)
            for population, result in results.items():
                try:
                    simulation_results[population].append(result) # each result should be a SpikeList object
                except KeyError:
                    simulation_results[population] = [result]

        if CACHE_RESULTS:
            cache = shelve.open("benchmarks_%s.cache" % mmodule, 'c')
            cache[str(benchmark_parameters.id)] = {'simulation_results': simulation_results}
            cache.close()

        # Analyse results
        print colour(yellow,"\nAnalysing results...")
        analyses = benchmark_parameters.analysis
        if not isinstance(analyses, list):
            analyses = [analyses]
        result = simulation_results
        for analysis in analyses:
            result = run_analysis(result, pcol, analysis, stimulus_variables)
            print colour(cyan,"    %s:" % (analysis.type)), str(result)[:500]
            if CACHE_RESULTS:
                cache = shelve.open("benchmarks_%s.cache" % mmodule, 'w')
                benchmark_data = cache[str(benchmark_parameters.id)]
                benchmark_data[analysis.type] = result
                cache[str(benchmark_parameters.id)] = benchmark_data
                cache.close()
        analysis_result = result

        # Compare to experimental data
        print colour(yellow, "\nComparison to experimental data...")
        if hasattr(pcol.comparison_data, "url"):
            comparison_data = pcol.comparison_data.url
            print colour(cyan, "    experimental data: %s" % comparison_data)
        else:
            comparison_data = (float(pcol.comparison_data.value), pcol.id)
            print colour(cyan, "    experimental data: %s %s" % (pcol.comparison_data.value, pcol.comparison_data.units))
        
        difference = compare(analysis_result,
                             comparison_data,
                             benchmark_parameters.difference_measure.type,
                             fig)
        difference *= float(pcol.weight)
        if single_plot:
            for result in analysis_result.values():
                try:
                    xdata.append(float(pcol.id))
                    ydata_result.append(result[0])
                    ydata_comparison.append(comparison_data[0])
                except ValueError:
                    pass
        
        print colour(bright+red,"%s difference:" % benchmark_parameters.difference_measure.type), difference
        
    if single_plot:
        panel = fig.panel(0)
        panel.plot1(xdata, ydata_result, 'ro-', label="Simulation results")
        panel.plot1(xdata, ydata_comparison, 'bs-', label="Experimental data")
        panel.legend(loc='best')

    # Finish up
    fig.save(str("%s_results" % benchmark_parameters.id))


def register(difference, xmlfile, modelfile):
    #This url is the page where this post request will be sent
    url = "http://127.0.0.1:8000/models/results_benchmarks/"

    username = raw_input("Please login with your BrainScaleS username and password.\nUsername: ")
    password = getpass()

    # Prepare the data
    query_args = { 'benchmark.name':os.path.basename(xmlfile)[:os.path.basename(xmlfile).find(".xml")], 'model.name':os.path.basename(modelfile)[:os.path.basename(modelfile).find(".py")], 'result' : difference, 'username' : username, 'password' : password}
 
    # This urlencodes your data (that's why we need to import urllib at the top)
    data = urllib.urlencode(query_args)
 
    # Send HTTP POST request
    request = urllib2.Request(url, data)


    try:
        response = urllib2.urlopen(request)
    except urllib2.HTTPError:
	print "Your username and password didn't match. Please try again."
	logged = False
	##The problem of this loop is that it will never end if the benchmark or the model used are not created, but there is no reason for this situation to happen
	while not logged:
	    try:
		username = raw_input("Username: ")
    		password = getpass()
		query_args['username'] = username
		query_args['password'] = password
		data = urllib.urlencode(query_args)
		request = urllib2.Request(url, data)
		response = urllib2.urlopen(request)
		logged = True
	    except urllib2.HTTPError:
		print "Your username and password didn't match. Please try again."
		logged = False


    webbrowser.open('http://127.0.0.1:8000/models/results_benchmarks/')


#===============================================================================

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print __doc__
    else:
        import urlparse
        xmlfile = sys.argv[1]
        modelfile = sys.argv[2]
        if urlparse.urlparse(xmlfile)[0] not in ('', 'file'):
            import ConfigParser
            config = ConfigParser.SafeConfigParser()
            config.read(os.path.expanduser("~/.benchmark_library"))
            auth_data = dict(config.items("server-login"))
            xmlfile, httpmsg = urllib.urlretrieve(xmlfile, data=urllib.urlencode(auth_data))
            if httpmsg.getsubtype() != 'xml':
                raise TypeError("Document retrieved does not appear to be XML (content type is %s)" % httpmsg.gettype())
        run_benchmark(xmlfile, modelfile)
        register(difference, xmlfile, modelfile)
