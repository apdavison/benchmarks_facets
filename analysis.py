"""
Module containing a library of analysis components.
The components are supposed to be chainable, i.e., the output from one
function should be the input to another.

$Id$
"""

from NeuroTools.analysis import TuningCurve
from NeuroTools.signals import SpikeList
import numpy
from matplotlib import mlab
import logging
from copy import deepcopy
try:
    import scipy.stats
    have_scipy = True
except ImportError:
    print "Warning: could not import scipy. Certain statistical tests will not be available."
    have_scipy = False

debug_fig = None


# == Analysis methods ==========================================================

def histogram(values, binwidth, minimum, maximum):
    bins = numpy.arange(minimum, maximum+binwidth/2.0, binwidth)
    hist = numpy.histogram(values, bins)
    if sum(hist[0]) < len(values):
        logging.warning("One or more values outside range")
        above = numpy.sum(values>maximum)
        below = numpy.sum(values<minimum)
        hist[0][0] += below
        hist[0][-1] += above
        assert sum(hist[0]) == len(values)
    return hist[0][:], hist[1][:-1] # bin edges contains the rightmost edge, which we don't want

def spike_histogram(allspiketimes, binwidth, duration):
    # needs updating = allspiketimes now contains a list of spiketrains, one per cell
    """
    Calculate the spike histogram from a list of spiketime arrays, one per
    repetition. The spike numbers are normalised to give firing rates in Hz.
    Spike times, binwidth and duration should all be in milliseconds.
    Returns a tuple (firing_rates,bins)
    """
    bins = numpy.arange(0,duration,binwidth)
    assert isinstance(allspiketimes,list)
    repetitions = len(allspiketimes)
    hist = numpy.zeros(len(bins))
    for spiketimes in allspiketimes:
        hist += nstats.histc(spiketimes,bins)*1000.0/binwidth
    hist /= repetitions
    return (hist,bins)

def mean_firing_rate(allspiketimes, duration):
    """
    Returns mean firing rate in spikes/s.
    Duration should be in ms.
    """
    assert isinstance(allspiketimes,list)
    repetitions = len(allspiketimes)
    nspikes = 0
    ncells = 0
    for spikelists in allspiketimes: # spikelists is a list of spiketrains, one per cell
        ncells += len(spikelists)
        for spikelist in spikelists:
            nspikes += len(spikelist)
    if ncells < 1:
        logging.warning("No cells recorded! Returning firing rate of zero.")
        return (0,)
    else:
        logging.debug("Mean firing rate = %d/%d/%d/%g = %g" % (nspikes, ncells, repetitions, duration/1000.0, nspikes/ncells/repetitions/(duration/1000.0)))
        return (nspikes/ncells/repetitions/(duration/1000.0),)

def find_preferred(spikelists, category_time_series):
    """
    In the scenario where some variable changes discontinously during the course
    of the recording, returns for each cell id the value of the variable at which
    the most spikes were recorded.
    The variable may take numeric values or string values (categories).
    """
    
    tuning_curves = {}
    for spikelist in spikelists:
        for cell_id, spike_train in spikelist.spiketrains.items():
            tc = spike_train.tuning_curve(category_time_series, normalized=False, method='mean')
            if tuning_curves.has_key(cell_id):
                tuning_curves[cell_id].add(tc)
            else:
                tuning_curves[cell_id] = TuningCurve(tc)
    logging.debug("  tuning_curves = %s" % str(tuning_curves))
    preferred = {}
    for cell_id, tc in tuning_curves.items():
        preferred[cell_id] = tc.max()[0]
    return preferred

def tuning_curves(spikelists, category_time_series, method='sum'):
    """
    In the scenario where some variable changes discontinously during the course
    of the recording, a tuning curve can be constructed in which spikes are
    binned according to the value of the variable at the time of the spike.

    `spikelists` is a list containing SpikeList objects, one for each repetition
      of the stimulus.
    `category_time_series` is a numpy array containing the variable value (the
      'category') at evenly spaced intervals between the start and stop times
      of the SpikeList objects (assumed to be the same for all objects).

    Returns a dictionary, indexed by cell id, whose values are dictionaries
    containing the mean tuning curve and stderr of the tuning curve for each cell.
    """
    tuning_curves = {}
    if not isinstance(spikelists, list):
        spikelists = [spikelists]
    for spikelist in spikelists:
        for cell_id, spike_train in spikelist.spiketrains.items():
            tc = spike_train.tuning_curve(category_time_series, normalized=False, method=method)
            if tuning_curves.has_key(cell_id):
                tuning_curves[cell_id].add(tc)
            else:
                tuning_curves[cell_id] = TuningCurve(tc)
    logging.debug("  tuning_curves = %s" % str(tuning_curves))
    stats = {}
    for cell_id, tuning_curve_obj in tuning_curves.items():
        mean, stderr = tuning_curve_obj.stats()
        stats[cell_id] = {'mean': mean, 'stderr': stderr}

    return stats

class Curve(object):
    pass


class HyperbolicRatio(Curve):
    
    def __init__(self, Rmax=1.0, x50=0.5, n=1.5 ):
        self.Rmax = Rmax
        self.x50 = x50
        self.n = n

    def estimate_initial_parameters(self, xdata, ydata):
        self.Rmax = max(ydata)
        self.x50 = numpy.median(xdata)
    
    def _set_params(self, values):
        self.Rmax, self.x50, self.n = values
    def _get_params(self):
        return self.Rmax, self.x50, self.n
    parameters = property(fget=_get_params, fset=_set_params)

    def function(self, x, parameters=None):
        if parameters is not None:
            try:
                Rmax, x50, n = parameters
            except ValueError:
                print "parameters =", parameters
                raise
        else:
            Rmax, x50, n = self.parameters
        xn = x**n
        return Rmax*xn/(x50**n + xn)
    
    def __str__(self):
        return "Curve: %g*x**%g/(%g**%g + x**%g)" % (self.Rmax, self.n, self.x50, self.n, self.n)

    def __repr__(self):
        return self.__str__()

class Triangle(Curve):

    def __init__(self, mu, cu, md, cd, peak_to_centre, spontaneous_response):
        self.mu = mu
        self.cu = cu
        self.md = md
        self.cd = cd
        self.peak_to_centre = peak_to_centre
        self.spontaneous_response = spontaneous_response

    def peak_location(self):
        if abs(self.mu) < numpy.inf and abs(self.md) < numpy.inf:
            xc = (self.cd - self.cu)/(self.mu - self.md)
        elif abs(self.mu) < numpy.inf: # downslope is vertical, cd should be set to the point where it crosses the x-axis
            xc = self.cd
        elif abs(self.md) < numpy.inf: # upslope is vertical
            xc = self.cu
        else: # both vertical
            assert self.cu == self.cd, "Inconsistent triangle with zero width"
            xc = self.cu
        logging.info("Intersection of fit lines is at %g degrees" % (xc-self.peak_to_centre))
        return xc-self.peak_to_centre

    def halfwidth(self):
        """
        Return the half-width at the level halfway between the spontaneous level
        and the intersection of the lines.
        """
        ys = self.spontaneous_response
        if abs(self.mu) < numpy.inf and abs(self.md) < numpy.inf:
            mhwhm = ((ys-self.cd)/self.md - (ys-self.cu)/self.mu)/4.0
        elif abs(self.mu) < numpy.inf: # downslope is vertical, cd should be set to the point where it crosses the x-axis
            mhwhm = (self.cd - (ys-self.cu)/self.mu)/4.0
        elif abs(self.md) < numpy.inf: # upslope is vertical
            mhwhm = ((ys-self.cd)/self.md - self.cu)/4.0
        else: # both vertical
            mhwhm = 0.0
        logging.info("Mean half-width at half-maximum: %g degrees" % mhwhm)
        if mhwhm < 0:
            errmsg = "Invalid half-width: %g  " % mhwhm 
            errmsg += "mu=%(mu)s, cu=%(cu)s, md=%(md)s, cd=%(cd)s, peak_to_centre=%(peak_to_centre)s, spontaneous=%(spontaneous_response)s" % self.__dict__
            errmsg += "peak_location=%s" % self.peak_location()
            raise Exception(errmsg)
        return mhwhm

def curve_fitting(tuning_curves, method, function_name, normalization=None):
    if normalization:
        norm_func = eval(normalization.replace(' ','_'))
    else:
        norm_func = lambda x: x
    curves = {}
    if method == "linear regression" and function_name == "triangle":
        for cell_id, tuning_curve in tuning_curves.items():
            logging.info("Calculating orientation tuning curve width for cell #%d" % cell_id)
            triangle = tuning_curve_fit_triangle(tuning_curve['mean'], tuning_curve['stderr'])
            curves[cell_id] = triangle
    elif method == "Levenberg-Marquardt" and function_name == "hyperbolic ratio":
        for cell_id, tuning_curve in tuning_curves.items():
            logging.info("Fitting tuning curve with %s function using %s method for cell #%d" % (function_name, method, cell_id))
            curve = HyperbolicRatio()
            tuning_curve['mean'].pop('null', None)
            xdata = tuning_curve['mean'].keys()
            xdata.sort()
            ydata = numpy.array([tuning_curve['mean'][X] for X in xdata], float)
            xdata = numpy.array(xdata, float)
            ydata = norm_func(ydata)
            curve.estimate_initial_parameters(xdata, ydata)
            curve = fit_levenberg_marquardt(xdata, ydata, curve)
            curves[cell_id] = curve
    else:
        raise Exception("Fitting method and/or curve type not yet implemented.")
    return curves

def peak_widths(curve_dict):
    """
    Given a dictionary containing Curve objects (usually with cell ids as
    dictionary keys, although we just throw these away, return a list containing
    the widths of the Curves.
    """
    widths = []
    for curve in curve_dict.values():
        widths.append(curve.halfwidth())
    return widths

def linreg(x,y):
    assert len(x) == len(y), "%s\n%s" % (x,y)
    A = numpy.ones((len(y), 2), dtype=numpy.float)
    A[:,0] = x
    return numpy.linalg.lstsq(A, y)

def tuning_curve_fit_triangle(tuning_curve, tuning_curve_stderr):
    """
    Given an orientation tuning curve, with standard errors for each datapoint,
    take all those points that are more than one stderr above the spontaneous
    level and that are contiguous to the peak of the curve. Fit separate
    straight lines to the upslope and the downslope*, and return a Triangle
    object.

    *the fit is done by linear regression. The stderrs are not used as weights -
    all points are weighted equally. Not clear if this is correct or not, need
    to check the original papers more carefully.

    See Rose and Blakemore (1974) Exp. Brain Res. 20: 1-17
    """
    orientations = tuning_curve.keys()
    orientations.remove("null")
    orientations = numpy.array(orientations, float)
    orientations.sort()

    spontaneous_response = tuning_curve['null']
    logging.info("Spontaneous response level: %g" % spontaneous_response)
    try:
        response = numpy.array([tuning_curve[theta] for theta in orientations], float)
        significant = numpy.array([tuning_curve[theta]-tuning_curve_stderr[theta] for theta in orientations], float)
    except KeyError: # if keys are strings
        response = numpy.array([tuning_curve[str(theta)] for theta in orientations], float)
        significant = numpy.array([tuning_curve[str(theta)]-tuning_curve_stderr[str(theta)] for theta in orientations], float)
    # offset orientations to get peak in the centre of the curve
    orientation_at_peak = orientations[response.argmax()]
    peak_to_centre = 90.0-float(orientation_at_peak)
    orientations = numpy.array(orientations, float) + peak_to_centre
    orientations = numpy.where(orientations>=180,orientations-180,orientations)
    orientations = numpy.where(orientations<0,orientations+180,orientations)

    # fit
    downslope = {'x':[], 'y':[]}; upslope = {'x':[], 'y':[]}
    for direction, slope in zip((-1,1),(upslope, downslope)):
        imax = response.argmax()
        n = len(significant)
        i = imax
        while significant[i] > spontaneous_response:
            slope['y'].append(response[i])
            slope['x'].append(orientations[i])
            i += direction
            if i == n:
                i = 0
            elif i == -1:
                i = n-1
            if i == imax:
                break
        slope['y'].append(response[i])
        slope['x'].append(orientations[i])
        slope['x'] = numpy.array(slope['x'])
        slope['y'] = numpy.array(slope['y'])
    logging.debug("  orientations: %s" % orientations)
    logging.debug("  response: %s" % response)
    logging.debug("  downslope: %s" % downslope)
    logging.debug("  upslope: %s" % upslope)

    if debug_fig: panel = debug_fig.next_panel()
    err_fmt = """%s significant data point%s in either upslope or downslope.
                 Spontaneous level    = %s
                 Response             = %s
                 Significant response = %s"""
    for slope in downslope, upslope:
        if len(slope['x'])>1:
            m, c = tuple(linreg(slope['x'], slope['y'])[0])
            slope['m'] = m; slope['c'] = c
            if debug_fig: 
                xdata = slope['x']-peak_to_centre
                xdata = numpy.where(xdata<0, xdata+180, xdata)
                panel.plot(xdata, m*slope['x'] + c, 'k', linewidth=2)
            logging.info("Fit: %gx + %g" % (m, c))
        elif len(slope['x']) == 1: # vertical line. Set m to inf and c to the x-intersect rather than the y-intersect
            slope['m'] = numpy.inf; slope['c'] = slope['x']
            logging.warning(err_fmt, "Only one", "", spontaneous_response, response, significant)
        else:
            errmsg = err_fmt % ("No", "s", spontaneous_response, response, significant)
            raise Exception(errmsg)
    cd = downslope['c']; cu = upslope['c']
    md = downslope['m']; mu = upslope['m']
    triangle = Triangle(mu,cu,md,cd,peak_to_centre,spontaneous_response)
    return triangle


def subtract_background(x):
    return x-x[0]

def residuals(function):
    def _residuals(p, x, y):
        err = y - function(x, p)
        return err
    return _residuals

def fit_levenberg_marquardt(xdata, ydata, curve):
    
    fit_params = scipy.optimize.leastsq(residuals(curve.function), curve.parameters,
                                        args=(xdata, ydata), full_output=True)
    if fit_params[-1] != 1: # fitting failed
        yfit = curve.function(xdata, fit_params[0])
        logging.warning("Fitting failed (%s)\nxdata: %s\nydata: %s\nyfit: %s" % (fit_params[-2], xdata, ydata, yfit))
    curve.parameters = fit_params[0]
    return curve
        
def filter_by_preferred(spikelists, category_time_series):
    """
    Find preferred for each cell and then filter the spike trains to
    leave only spikes that occur while the variable has that value.
    """
    logging.info("Filtering %d spikelists with a category_time_series that begins with %s and ends with %s" % (len(spikelists), category_time_series[:10], category_time_series[-10:]))    
    category_time_series = numpy.array(category_time_series)
    preferred_values = find_preferred(spikelists, category_time_series)
    logging.debug("  Preferred values are: %s" % preferred_values)
    
    # Check that t_start and t_stop is the same for all spikelists
    t_start = spikelists[0].t_start
    t_stop = spikelists[0].t_stop
    for spikelist in spikelists:
        assert spikelist.t_start == t_start and spikelist.t_stop == t_stop
    interval = (t_stop-t_start)/len(category_time_series)
    logging.debug("  t_start = %g, t_stop = %g, interval = %g" % (t_start, t_stop, interval))
    
    filtered_spikelists = []
    for spikelist in spikelists:
        filtered_spikelist = SpikeList([], [])
        initialised = False
        for cell_id, spiketrain in spikelist.spiketrains.items():
            preferred_value = preferred_values[cell_id]
            mask = (category_time_series==preferred_value)
            # Calculate times at which the category has the preferred value
            time_points = numpy.arange(t_start, t_stop, interval)
            start_times = time_points[mask]
            time_points = numpy.roll(time_points, -1)
            time_points[-1] = time_points[-2] + interval
            stop_times = time_points[mask]
            assert stop_times[-1] > start_times[-1]
            # Filter the spiketrain and add to the new spikelist
            filtered_spiketrain = spiketrain.time_slice(start_times, stop_times)
            filtered_spiketrain.relative_times()
            if not initialised:
                filtered_spikelist.t_start = filtered_spiketrain.t_start
                filtered_spikelist.t_stop = filtered_spiketrain.t_stop
                initialised = True
            filtered_spikelist.append(cell_id, filtered_spiketrain)

        filtered_spikelists.append(filtered_spikelist)
    return filtered_spikelists

def merge_trials(spikelists, relative_times=True):
    """
    Merge all the spikelists into a single spikelist (i.e. for all spike trains
    that have the same id in each spikelist, merge the spike trains into a
    single spike train.
    """
    merged_spikelist = deepcopy(spikelists[0]) # don't want to modify the original data, so deepcopy
    if len(spikelists) > 1:
        for spikelist in spikelists[1:]:
            merged_spikelist.merge(spikelist, relative=relative_times) # overlay all trials by making all spiketimes relative to t_start (i.e. setting t_start to 0)
    return merged_spikelist

def f1f0_ratio(spikelist, bin_width, f_stim):
    """
    Calculates the F1/F0 ratios for stimulus frequency `f_stim`.
    
    """
    return spikelist.f1f0_ratios(bin_width, f_stim).values()

def extract_parameter(curve_dict, name):
    values = []
    for curve in curve_dict.values():
        values.append(getattr(curve, name))
    return numpy.array(values)

# == Difference measures =======================================================

def rms_error(xdata1, ydata1, xdata2, ydata2, panel):
    """
    The experimental data should be passed as xdata1,ydata1, the model results
    as xdata2, ydata2.
    The value of ydata2 is calculated by interpolation at each value of xdata1,
    and the rms difference between ydata1 and the interpolated ydata2 is
    returned.
    """
    ydata2i = mlab.stineman_interp(xdata1, xdata2, ydata2)
    panel.plot(xdata1, ydata2i)
    assert len(ydata1)==len(ydata2i)
    err = numpy.sqrt(numpy.sum(numpy.square(ydata1-ydata2i))/len(ydata1))
    return err

def chi_square(xdata1, ydata1, xdata2, ydata2, panel):
    """
    The experimental data should be passed as xdata1,ydata1, the model results
    as xdata2, ydata2.
    xdata1 and xdata2 should be identical.
    """
    def rebin(ydata1, ydata2):
        new_ydata1 = []; new_ydata2 = []; y2_sum = 0
        for y1,y2 in zip(ydata1, ydata2):
            y2_sum += y2
            if y1 != 0:
                new_ydata1.append(y1)
                new_ydata2.append(y2_sum)
                y2_sum = 0
        assert sum(new_ydata1) == sum(ydata1), "%s != %s" % (sum(new_ydata1), sum(ydata1))
        assert sum(new_ydata2) == sum(ydata2), "%s != %s" % (sum(new_ydata2), sum(ydata2))
        return numpy.array(new_ydata1), numpy.array(new_ydata2)
    
    logging.info("Running chi-square test")
    assert len(xdata1) == len(xdata2) == len(ydata1) == len(ydata2), "%s, %s %s %s" % (len(xdata1), len(xdata2), len(ydata1), len(ydata2))
    assert all(numpy.array(xdata1)-numpy.array(xdata2)<1e-12), str(numpy.array(xdata1)-numpy.array(xdata2))
    
    if sum(ydata1) != sum(ydata2): # normalize model distribution to have same number of events as experimental data
        logging.warning("Normalising model distribution (%d events) to have same number of events as experimental data (%d events)" % (sum(ydata2),sum(ydata1)))
        ydata2 = numpy.array(ydata2)/float(sum(ydata2))*sum(ydata1)
    # if experimental data (ydata1) contains zeros, need to re-bin both datasets
    if 0 in ydata1:
        logging.warning("Experimental data contains zero-frequency values. Re-binning.")
        logging.debug("  Original data: %s\n                 %s" % (ydata1, ydata2))
        ydata1, ydata2 = rebin(ydata1, ydata2)
        logging.debug("  Re-binned data: %s\n                  %s" % (ydata1, ydata2))
    if have_scipy:
        return scipy.stats.chisquare(f_obs=ydata2, f_exp=ydata1)[0]
    else:
        logging.warning("Warning: scipy not available. Returning a fake value.")
        return 99999


