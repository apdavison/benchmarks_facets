import simpleV1
import logging
import Benchmarks.analysis
from math import pi
from copy import deepcopy
from NeuroTools.plotting import SimpleMultiplot
import numpy
from pprint import pprint

P = simpleV1.P
stimulus_url = "file:////data/andrew/Benchmarks/VisualStimuli/multiple_gratings_20080804-1034.zip"
scale_factor = 50.0
repetitions = 5
bin_size = 50.0
f_stim = 3.3 # Hz
display = True
max_luminance = None
background_luminance = None
MAX_COL = 1

def filter_stimulus_variables(stimulus_variables, all_vars, use_vars):
    filtered_variables = deepcopy(stimulus_variables)
    mask = []
    for var in use_vars:
        mask.append(all_vars.index(var))
    for i,line in enumerate(filtered_variables):
        entries = line.split(',')
        if len(entries) > 1:
            filtered_variables[i] = ",".join(numpy.array(line.split(','))[mask])
    return filtered_variables

if display:
    import pylab
    pylab.rcParams['interactive'] = True
    pylab.rcParams['ytick.labelsize'] = 4
    pylab.rcParams['xtick.labelsize'] = 6
    
    spike_fig = SimpleMultiplot(P['n_cells']+3, min(MAX_COL, repetitions))
    filtered_fig = SimpleMultiplot(P['n_cells']+3, min(MAX_COL, repetitions))
    merged_fig = SimpleMultiplot(P['n_cells']+3, min(MAX_COL, repetitions))

    def plot_spike_hist(fig, spike_list, with_variables=True):
        #spike_hist = spike_list.spike_histogram(bin_size, normalized=False, display=False)
        #time = pylab.arange(spike_hist.shape[1])*bin_size
        time = spike_list.time_axis(bin_size)
        spike_hist = spike_list.spike_histogram(time, normalized=False, display=False)
        y_max = spike_hist.max()
        x_max = time.max()
        for row in spike_hist:
            panel = fig.next_panel()
            panel.plot1(time, row)
            panel.set_xlim(0, x_max)
            panel.set_ylim(0, y_max)
            panel.set_yticks(range(0, y_max, 5))
        if with_variables:
            for i, label, y_max in [(0, 'Contrast', 1.0), (1, 'Orientation (deg)', 135), (2, 'Spatial freq. (cycles/deg)', 20.0)]:
                var_panel = fig.next_panel()
                values = [((v!='null') and eval(v)[i] or None) for v in frame_variables]
                var_panel.plot1(time, values)
                var_panel.set_xlim(0, x_max)
                var_panel.set_ylim(0, y_max)
                var_panel.set_ylabel(label, fontsize=4)
                #var_panel.set_yticks([0,y_max])
            

simpleV1.record('spikes', {'not specified': {'not specified': P['n_cells']}})

frame_variables = simpleV1.set_stimulus(stimulus_url, scale_factor, max_luminance, background_luminance)
results = []

for i in range(repetitions):
    logging.info("Trial #%d" % i)
    spike_list = simpleV1.run(quiet=False).values()[0]
    logging.debug("  Spike train for cell #0: %s" % str(spike_list[0]))
    results.append(spike_list)
    if display and i < MAX_COL:
        plot_spike_hist(spike_fig, spike_list)
        spike_fig.save("/home/andrew/tmp/test_contrast_response_spikes.png")
    
all_vars = ['contrast', 'orientation', 'spatial_freq']
print "frame_variables[-10:] = ", frame_variables[-10:]
filtered_variables = filter_stimulus_variables(frame_variables, all_vars, ['orientation', 'spatial_freq'])
print "filtered_variables[-10:] = ",filtered_variables[-10:]
preferred = Benchmarks.analysis.find_preferred(results, filtered_variables)
##preferred = Benchmarks.analysis.find_preferred(results, frame_variables, mask=slice(1,3))
print "Preferred = ", preferred

# Compare measured to actual preferred orientation
for cell_id, pref in preferred.items():
    print cell_id, simpleV1.cells[cell_id].theta*180.0/pi, pref

filtered_results = Benchmarks.analysis.filter_by_preferred(results, filtered_variables)
##filtered_results = Benchmarks.analysis.filter_by_preferred(results, frame_variables, mask=slice(1,3))

if display:
    for spikelist in filtered_results[:MAX_COL]:
        plot_spike_hist(filtered_fig, spikelist, with_variables=True)
        filtered_fig.save("/home/andrew/tmp/test_contrast_response_filtered.png")

# merge, then f1f0_ratios
merged_results = Benchmarks.analysis.merge_trials(filtered_results, relative_times=False)

if display:
    plot_spike_hist(merged_fig, merged_results, with_variables=True)
    merged_fig.save("/home/andrew/tmp/test_contrast_response_merged.png")

contrast_times = filter_stimulus_variables(frame_variables, all_vars, ['contrast'])
print "contrast_times[-10:]:", contrast_times[-10:]
tuning_curves = Benchmarks.analysis.tuning_curves([merged_results], contrast_times, 'mean')
pprint(tuning_curves)

fitting_result = Benchmarks.analysis.curve_fitting(tuning_curves,
                                                   "Levenberg-Marquardt",
                                                   "hyperbolic ratio",
                                                   normalization="subtract background")
if display:
    curve_fig = SimpleMultiplot(P['n_cells'], 1, xlabel="Contrast", ylabel="Response",
                                scaling=('log','linear'))
    for id in fitting_result.keys():
        tuning_curve = tuning_curves[id]
        xdata = tuning_curve['mean'].keys()
        xdata.sort()
        ydata = Benchmarks.analysis.subtract_background([tuning_curve['mean'][X] for X in xdata])
        xdata = numpy.array(xdata, 'float')
        
        fit_curve = fitting_result[id]
        xfit = numpy.exp(numpy.arange(numpy.log(0.01), numpy.log(1.0), (numpy.log(0.01)-numpy.log(1.0))/-30.0))
        yfit = fit_curve.function(xfit)

        panel = curve_fig.next_panel()
        panel.plot1(xdata, ydata, 'bo', xfit, yfit, 'r-')
    curve_fig.save("/home/andrew/tmp/test_contrast_response_curves.png")

pprint(fitting_result)

exponents = Benchmarks.analysis.extract_parameter(fitting_result, 'n')

print exponents

exponent_hist = Benchmarks.analysis.histogram(exponents, 0.3, 0.15, 7.95)
print exponent_hist
if display:
    hist_fig = SimpleMultiplot(1, 1, xlabel="Exponent (n)", ylabel="Number of cells")
    hist_fig.next_panel().plot(exponent_hist[1], exponent_hist[0])
    hist_fig.save("/home/andrew/tmp/test_contrast_response_histogram.png")