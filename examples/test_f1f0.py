
import simpleV1
import logging
import Benchmarks.analysis
from math import pi
from copy import deepcopy
from NeuroTools.plotting import SimpleMultiplot

P = simpleV1.P
stimulus_url = "file:////data/andrew/Benchmarks/VisualStimuli/multiple_gratings_20080731-1631.zip"
scale_factor = 50.0
repetitions = 10
bin_size = 50.0
f_stim = 1.0 # Hz
display = True
max_luminance = None
background_luminance = None
MAX_COL = 2

if display:
    import pylab
    pylab.rcParams['interactive'] = True
    pylab.rcParams['ytick.labelsize'] = 4
    pylab.rcParams['xtick.labelsize'] = 6
    
    spike_fig = SimpleMultiplot(P['n_cells']+2, min(MAX_COL, repetitions))
    filtered_fig = SimpleMultiplot(P['n_cells'], min(MAX_COL, repetitions))
    merged_fig = SimpleMultiplot(P['n_cells'], min(MAX_COL, repetitions))

    def plot_spike_hist(fig, spike_list, with_variables=True):
        spike_hist = spike_list.spike_histogram(bin_size, normalized=False, display=False)
        time = pylab.arange(spike_hist.shape[1])*bin_size
        y_max = spike_hist.max()
        x_max = time.max()
        for row in spike_hist:
            panel = fig.next_panel()
            panel.plot1(time, row)
            panel.set_xlim(0, x_max)
            panel.set_ylim(0, y_max)
            panel.set_yticks(range(0, y_max, 5))
        if with_variables:
            for i, label, y_max in [(0, 'Orientation (deg)', 135), (1, 'Spatial freq. (cycles/deg)', 20.0)]:
                var_panel = fig.next_panel()
                values = [((v!='null') and eval(v)[i] or None) for v in frame_variables]
                var_panel.plot1(time, values)
                var_panel.set_xlim(0, x_max)
                var_panel.set_ylim(0, y_max)
                var_panel.set_ylabel(label, fontsize=4)
                #var_panel.set_yticks([0,y_max])
            

simpleV1.record('spikes', {'not specified': {'not specified': P['n_cells']}})

frame_variables = simpleV1.set_stimulus(stimulus_url, scale_factor, max_luminance, background_luminance)
print "frame_variables[-10:] = ", frame_variables[-10:]
results = []

for i in range(repetitions):
    logging.info("Trial #%d" % i)
    spike_list = simpleV1.run(quiet=False).values()[0]
    logging.debug("  Spike train for cell #0: %s" % str(spike_list[0]))
    results.append(spike_list)
    if display and i < MAX_COL:
        plot_spike_hist(spike_fig, spike_list)
        spike_fig.save("/home/andrew/tmp/test_f1f0_spikes.png")
    
preferred = Benchmarks.analysis.find_preferred(results, frame_variables)

# Compare measured to actual preferred orientation
for cell_id, pref in preferred.items():
    print cell_id, simpleV1.cells[cell_id].theta*180.0/pi, pref

filtered_results = Benchmarks.analysis.filter_by_preferred(results, frame_variables)

if display:
    for spikelist in filtered_results[:MAX_COL]:
        plot_spike_hist(filtered_fig, spikelist, with_variables=False)
        filtered_fig.save("/home/andrew/tmp/test_f1f0_filtered.png")

# merge, then f1f0_ratios
merged_results = Benchmarks.analysis.merge_trials(filtered_results)

if display:
    plot_spike_hist(merged_fig, merged_results, with_variables=False)
    merged_fig.save("/home/andrew/tmp/test_f1f0_merged.png")

f1f0_ratios = Benchmarks.analysis.f1f0_ratio(merged_results, bin_size, f_stim)

print f1f0_ratios
