
import logging
import sys

# == Coloured text on console ==================================================
red     = 0010; green  = 0020; yellow = 0030; blue = 0040;
magenta = 0050; cyan   = 0060; bright = 0100
try:
    import ll.ansistyle
    def colour(col,text):
        return str(ll.ansistyle.Text(col,str(text)))
except ImportError:
    def colour(col,text):
            return text
        
# == Default setup for logging module ==========================================
logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(message)s ',
                    filename='python.log',
                    filemode='w')
console = logging.StreamHandler()
console.setLevel(logging.INFO)
console.setFormatter(logging.Formatter('%(message)s'))
logging.getLogger('').addHandler(console)

def progress_bar(progress):
    length = 50
    filled = int(round(length*progress))
    print "|" + "="*filled + " "*(length-filled) + "|\r",
    sys.stdout.flush()
    

    