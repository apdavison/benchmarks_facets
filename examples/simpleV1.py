"""
Simple V1 model

Andrew P. Davison, UNIC, CNRS, November 2007
"""

##from NeuroTools.facets import fkbtools
from NeuroTools import parameters
from NeuroTools.stgen import StGen
from NeuroTools.signals import SpikeTrain, SpikeList
import shutil
import os
import atexit
import numpy
import matplotlib
from numpy import sin, cos, exp, pi, sqrt
from myutilities import *
from Benchmarks import plotting
import Benchmarks.analysis
import tempfile, zipfile, urllib
from zipfile import ZipFile


# Global variables (to be set later)
frame_duration = None
sim_duration = None
images = None 
recordedvariable = None
tmpdir = None
cells = []
orientations = None
stimulus_id = ''
cells_cache = None
figures = {}

# === Define interface for visionbenchmark.py ==================================

def record(measureable, brain_region_dict):
    global recordedvariable, cells
    recordedvariable = measureable
    layer = brain_region_dict.keys()[0]
    assert layer == 'not specified', "Recording from specific layers not yet supported."
    cell_type = brain_region_dict[layer].keys()[0]
    assert cell_type == 'not specified', "Recording from specific cell types not yet supported."
    n_cells = brain_region_dict[layer][cell_type]
    assert isinstance(n_cells, int), "n_cells must be an integer, not %s" % type(n_cells)
    logging.info("Recording %s" % recordedvariable)
    # Build the model - doing it here allows the script to be run standalone or via visionbenchmark.py
    cells = build(n_cells, P['gabor'], P['non_linearity'])
    if P['debug']:
        n_cells = min(n_cells, 10*6) #15*9)
        ncols = int(sqrt(n_cells/0.6)); nrows = n_cells/ncols
        if n_cells%ncols != 0: nrows += 1
        figures['orientations'] = plotting.SimpleMultiplot(nrows, ncols, xlabel="Orientation (deg)", ylabel="Number of spikes")
        figures['responses'] = plotting.SimpleMultiplot(nrows, ncols, xlabel="Time (ms)", ylabel="Response")
        figures['gabors'] = plotting.SimpleMultiplot(nrows, ncols)
        Benchmarks.analysis.debug_fig = figures['orientations']

##This is the function that replace fkbtools

def getstimulus(url):
    """
    Get the stimulus in either zip(png) or hdf5 format, unpack individual frames
    into a temporary directory, and return the temp directory, a file containing
    a list of frames, and the frame duration.
    """
    tmpdir1 = tempfile.mkdtemp()
    if url[-3:] == ".h5":
        try:
            hdf5_to_png(url,"file://%s/tmpstim.zip" % tmpdir1)
            url = "%s/tmpstim.zip" % tmpdir1
        except IOError: # not an hdf5 file, or some other problem.
            pass

    tmpdir = "/home/stud/Projet_CNRS/benchmarks_site/media/stimuli/"
    tmpfile = tmpdir + "tmpfile.zip"
    urllib.urlretrieve(url, tmpfile)
    zipTest = ZipFile(tmpfile)
    zipTest.extractall(tmpdir)
    dirlist = [f for f in os.listdir(tmpdir) if f.find(".flv") < 0 and f.find("MACOSX") < 0 and f.find(".zip") < 0]
    if len(dirlist) == 1:
        topdir = os.path.join(tmpdir, dirlist[0])
	tmpdir = topdir
    else:
        raise Exception("Problem obtaining stimulus. Contents of %s: %s" % (tmpdir, os.listdir(tmpdir)))
    frame_list = (os.path.join(topdir,"frames"),topdir)
    f = open(os.path.join(topdir,"parameters"),'r') # parameteters should contain the frame_duration parameter
    for line in f.readlines():
        if line.find('frame_duration') == 0:
            exec(line)
            break
    f.close()
    shutil.rmtree(tmpdir1)
    if tmpfile is not None:
        os.remove(tmpfile)
    return (tmpdir,frame_list,frame_duration)


def set_stimulus(stimulus_url, scale_factor, max_luminance, background_luminance):
    global frame_duration, images, orientations, tmpdir, stimulus_id
    logging.info("Setting stimulus from %s" % stimulus_url)
    logging.info("Scale factor: %g pixels/degree" % scale_factor)
##    tmpdir, image_sequence, frame_duration = fkbtools.getStimulus(stimulus_url)
    tmpdir, image_sequence, frame_duration = getstimulus(stimulus_url)
    logging.info("Frame duration = %g ms" % frame_duration)
    images, frame_variables = load_frames(image_sequence)
    stimulus_id = stimulus_url
    return frame_variables
    
def run(quiet=True):
    global cells, frame_duration, images, tmpdir, sim_duration, orientations, time_bins
    # Run the model
    if recordedvariable is None:
        raise Exception("You must call record(<variable_to_record>) before run()")
    elif images is None or frame_duration is None:
        raise Exception("You must call set_stimulus(<stimulus_file>,<scale_factor>) before run()")
    else:
        if not quiet:
            logging.info("Running V1 model...")
            
        sim_duration = frame_duration * len(images)
        present_stimulus(cells, images, orientations, frame_duration)
        time_bins = numpy.arange(0.0, len(images)*frame_duration, frame_duration)
        
    # Return the results
    if recordedvariable == "spikes":
        result = SpikeList([], [])
        for cell in cells:
            result[cell.id] = cell.spike_train
    else:
        raise "Can only record spikes"
    # Return the result
    return {'not specified': result}

def clean_up() :
    """Delete temporary files."""
    global tmpdir, frame_duration, sim_duration, images, recordedvariable, cells, orientations, stimulus_id, cells_cache, figures
    if tmpdir is not None:
        shutil.rmtree(tmpdir)
    if P['debug']:
        if os.path.exists(P['output_dir']): 
            for label,fig in figures.items():
                fig.save(os.path.join(P['output_dir'], "simpleV1_%s.png" % label))
        else:
            print "Output directory %s does not exist." % P['output_dir']


atexit.register(clean_up)


# === Define model =============================================================

"""
# Get list of image files and orientations
images, orientations = load_frames(image_sequence)
# Create cells
cells = build(n_cells, gabor_parameters, non_linearity_parameters)
# Present stimuli to the cells
present_stimulus(cells, images, orientations, frame_duration)
"""

def gabor(x, y, theta, gamma, sigma, k, psi):
    xp = x*cos(theta) + y*sin(theta)
    yp = -x*sin(theta) + y*cos(theta)
    return exp(-(xp*xp + gamma*gamma*yp*yp)/(2*sigma*sigma))*cos(2*pi*k*xp + psi)

def sigmoid(x,x0,k):
    return 1/(1+exp(-k*(x-x0)))

def load_frames(image_sequence):
    """
    Read image filenames and the frame variable value(s) for each frame.
    
    `image_sequence` should be a tuple containing the filename of the sequence
    file and the directory containing the image files.
    """
    if isinstance(image_sequence,tuple): # file listing frames
        sequence_file, image_dir = image_sequence
        f = open(sequence_file, 'r')
        lines = f.readlines()
        f.close()
    else:
        raise Exception("image_sequence must be a tuple (sequence_file, image_dir)")
    ncol = len(lines[0].split())
    if ncol == 1:
        return ["%s/%s" % (image_dir, line.strip()) for line in lines], []
    elif ncol > 1:
        images = ["%s/%s" % (image_dir, line.split()[0]) for line in lines]
        frame_variables = [line.split()[1:] for line in lines]
        for i in range(len(frame_variables)):
            frame_variables[i] = ", ".join(frame_variables[i])
        return images, frame_variables        
    else:
        raise Exception("Invalid frames file")

def present_stimulus(cells, images, orientations, frame_duration):
    """
    Present the stimulus and generate spiketrains.
    
    If this is the first time that function has been called, the cells are
    created and the image frames presented to them to calculate the `x` and `y`
    variables. These cells are then cached.
    
    On subsequent calls to the function, `cells` and `images` are ignored and
    the cached cells used. However, a new spike train is generated by each cell.
    
    (`orientations` does not seem to be used).
    """
    global stimulus_id, cells_cache
    if cells_cache is not None:
        cells = cells_cache
        logging.info("Retrieving cached cells")
    else:
        logging.debug("  images[0:2] = %s" % images[0:2])
        logging.debug("  images[-3:-1] = %s" % images[-3:-1])
        assert os.path.isfile(images[0]), "%s is not a file or does not exist" % images[0]
        img0 = matplotlib.image.imread(images[0])
        width_pixels = img0.shape[0]
        height_pixels = img0.shape[1]
        logmsg = "Frame size: %d x %d   " % (height_pixels, width_pixels)
        logmsg += "Number of frames: %d   " % len(images)
        logmsg += "Frame duration: %g ms" % frame_duration
        logging.info(logmsg)

        for cell in cells:
            cell.setup(len(images), frame_duration)
            
        logging.info("Presenting image frames to %d cells and calculating response." % len(cells))
        for i,filename in enumerate(images):
            img = matplotlib.image.imread(filename)
            img = img[:,:,0:3].mean(axis=2) # rgb --> greyscale
            for cell in cells:
                cell.present_image(img)
        cells_cache = cells
        
    logging.info("Generating spikes for %d frames each of duration %g ms" % (len(images),frame_duration))
    time_bins = numpy.arange(0.0, len(images)*frame_duration, frame_duration)
    for cell in cells:
        cell.generate_spikes(time_bins)

def build_kernel(kernel_size, theta, gamma, sigma, k, psi, normalized=True):
    """
    Create a Gabor-filter kernel
      `kernel_size` is the length of the sides of the (square) kernel in pixels
      `k` represents the spatial frequency of the stripes,
      `theta` represents the orientation of the stripes,
      `psi` is the phase offset,
      `gamma` specifies the ellipticity.
    If `normalized`, the sum of the elements of the kernel is set to zero
    (by subtraction of the mean value.
    """
    offset = kernel_size/2
    f = lambda x,y: gabor(x-offset, y-offset,
                          theta=theta,
                          gamma=gamma,
                          sigma=sigma*kernel_size,
                          k=k/float(kernel_size),
                          psi=psi)
    kernel = numpy.fromfunction(f, (kernel_size, kernel_size))
    
    if normalized: # kernel should sum to zero
        sum = kernel.sum()
        kernel -= sum/kernel_size**2
    return kernel

def convolve(img, kernel):
    """
    Convolve the part of the image within the receptive field, as given by the
    kernel size, with the kernel. The receptive field is always centred within
    the image.
    """
    # all units are in pixels
    img_height = img.shape[0]
    img_width = img.shape[1]
    kernel_height = kernel.shape[0]
    kernel_width = kernel.shape[1]
    assert kernel_height <= img_height
    assert kernel_width <= img_width
    #for x in img_height, img_width, kernel_height, kernel_width:
    #    assert x%2 == 1
    margin_tb = int((img_height-kernel_height)/2.0)
    margin_lr = int((img_width-kernel_width)/2.0)
    
    cropped_img = img[margin_tb:margin_tb+kernel_height,
                      margin_lr:margin_lr+kernel_width]
    return (cropped_img * kernel).sum()

def build(n_cells, gabor_parameters, non_linearity_parameters):
    """
    Create a list of `GaborCell`s with identical parameters except for
    orientation.
    """
    logging.info("Creating %d GaborCells" % n_cells)
    logging.debug("  gabor_parameters = %s" % gabor_parameters)
    logging.debug("  non_linearity_parameters = %s" % non_linearity_parameters)
    cells = []
    for i in range(n_cells):
        cells.append( GaborCell(gabor_parameters, non_linearity_parameters) )
    return cells

def plot_hist(panel, hist, bins, width):
    for t,n in zip(bins,hist):
        panel.bar(t,n,width=width)


class GaborCell(object):
    """
    A spike generator with a Gabor-filter receptive field.
    
    It has two variables: `x` is something like the membrane potential and is
    obtained by convolving the part of the current image frame within the
    receptive field with the Gabor kernel. `y` is the instantanous firing rate
    and is obtained by applying a sigmoid function to `x`.
    
    The cell produces an inhomogeneous Poisson spike train, accessible as the
    attribute `spike_train`, with instantaneous firing rate given by y.
    
    Usage::
    
        gc = GaborCell(gabor_parameters, non_linearity_parameters)
        gc.setup(nframes, frame_duration)
        for img in list_of_image_frames:
            gc.present_image(img)
        gc.generate_spikes(time_bins)
        
    """
    
    highest_id = -1
    
    def __init__(self, gabor_parameters, non_linearity_parameters):
        """
        `gabor_parameters` should contain parameters named `'gamma'`,
        `'sigma'`, `'k'`, `'psi'`, `'kernel_size'` and `'normalized'`.
        See `build_kernel()` for a definition of the parameters.
        
        `non_linearity_parameters` should contain parameters named `'x0'`,
        `'k'` and `'gain'`.
        
        The Gabor orientation is chosen randomly.
        """
        GaborCell.highest_id += 1
        self.id = GaborCell.highest_id
        for name,val in gabor_parameters.items():
            setattr(self, name, val)
        self.theta = numpy.random.uniform(0, pi)
        logging.info("Cell #%d has orientation %5.1f degrees" % (self.id, self.theta*180/pi))
        self.kernel = build_kernel(self.kernel_size, self.theta, self.gamma,
                                   self.sigma, self.k, self.psi, self.normalized)
        logging.debug("  kernel max = %g, min = %g (cell %d)" % (self.kernel.max(), self.kernel.min(), self.id))
        for name,val in non_linearity_parameters.items():
            setattr(self, name, val)
        self.spike_generator = StGen()
        
    def setup(self, nframes, frame_duration):
        """Prepare the cell to receive input."""
        self.x = numpy.zeros((nframes,), 'float')
        self.y = numpy.zeros((nframes,), 'float')
        self.orientation_tuning_curve = {}
        self.sim_duration = nframes*frame_duration
        self.frame_id = 0

    def present_image(self, img):
        """Present an image frame to the cell and calculate `x` and `y`."""
        i = self.frame_id
        self.x[i] = convolve(img, self.kernel)
        self.y[i] = self.gain * sigmoid(self.x[i], self.x0, self.k)
        self.frame_id += 1

    def generate_spikes(self, time_bins):
        """Once the `y` array has been calculated, generate the spike array."""
        if P['debug']:
            try:
                panel = figures['gabors'].panel(self.id)
                panel.imshow(self.kernel)
            except IndexError:
                pass
            try:
                panel = figures['responses'].panel(self.id)
                panel.plot(time_bins, self.x)
                panel.plot(time_bins, self.y)
            except IndexError:
                pass
        assert len(time_bins) == len(self.y)
        self.spike_train = self.spike_generator.inh_poisson_generator(
                              t=time_bins, rate=self.y, t_stop=self.sim_duration) # times in ms, rate in spikes/ms
        logging.debug("  Generating spikes for GaborCell %d: %d spikes" % (self.id, len(self.spike_train)))
        logging.debug("  Maximum firing rate: %g spikes/s" % self.y.max())

    def spontaneous_firing_rate(self):
        """Return the value of `y` that would be obtained for zero input."""
        return self.gain * sigmoid(0.5*self.kernel.sum(), self.x0, self.k)


# === Read parameter file on import ============================================

parameter_file = "simpleV1.param"
curdir,pyc = os.path.split(os.path.abspath(__file__))
P = parameters.ParameterSet(os.path.join(curdir,parameter_file))
console.setLevel(logging.WARNING)

# === If run as a standalone script, run the model and analysis ================

if __name__ == "__main__":
    if P['debug']:
        matplotlib.rcParams['interactive'] = True
    ##Needed for me to run on my computer because I didn't have the identification codes
    #stimulus_url = "srb://facets.inria.fr/WP5/Benchmarks/VisualStimuli/moving_gbar10_w0.10_c0.5_v10.0.zip"
##    stimulus_url = "file:///data/andrew/Benchmarks/VisualStimuli/moving_gbar10_w0.10_c0.5_v10.0.zip"
    stimulus_url = "/home/stud/Projet_CNRS/benchmarks_site/media/stimuli/moving_gbar10_w0.10_c0.5_v10.0.zip"
    scale_factor = 10.0 # pixels/degree
    repetitions = 10
    halfwidth_binwidth = 2.5
    halfwidth_max = 70

    record('spikes', {'not specified': {'not specified': P['n_cells']}})
    orientations = set_stimulus(stimulus_url, scale_factor, None, None) # this sets orientations as a global, but could also return orientations?
    results = []
    for i in range(repetitions):
        logging.info("Trial #%d" % i)
        spike_list = run(quiet=False).values()[0]
        logging.debug("  Spike train for cell #0: %s" % str(spike_list[0]))
        results.append(spike_list)
    
    # Analyse results
    tuning_curves = Benchmarks.analysis.tuning_curves(results, orientations)
    
    if P['debug']:
        xdata = tuning_curves[0]['mean'].keys()
        xdata.remove("null")
        xdata.sort()
        for cell_id, tuning_curve in tuning_curves.items():
            panel = figures['orientations'].next_panel()
            ydata = [tuning_curve['mean'][x] for x in xdata]
            yerr  = [tuning_curve['stderr'][x] for x in xdata]
            panel.errorbar(xdata, ydata, yerr)
    
    # Fit triangles to tuning curves
    widths = []
    peaks = []
    if P['debug']:
        figures['orientations']._curr_panel = 0
    for cell_id, tuning_curve in tuning_curves.items():
        logging.info("Calculating orientation tuning curve width for cell #%d" % cell_id)
        triangle = Benchmarks.analysis.tuning_curve_fit_triangle(tuning_curve['mean'], tuning_curve['stderr'])
        hwhm = triangle.halfwidth()
        peak = triangle.peak_location()
        logging.info("Half-width at half-maximum for cell #%d: %g degrees" % (cell_id, hwhm))
        widths.append(hwhm)
        peaks.append(peak)
        
    # Calculate histogram of tuning curve widths
    hist, bins = Benchmarks.analysis.histogram(widths, halfwidth_binwidth, 0, halfwidth_max)
    logging.info("Tuning curve width distribution:")
    logging.info("%s" % bins)
    logging.info("%s" % hist)
    if P['debug']:
        figures['width_histogram'] = plotting.SimpleMultiplot(1, 1, xlabel="Average half-width (deg)", ylabel="Number of cells")
        panel = figures['width_histogram'].next_panel()
        plot_hist(panel, hist, bins, width=2.0)
    
    # Plot accuracy of peak estimation
    if P['debug']:
        real_peaks = [cell.theta*180/pi for cell in cells]
        figures['peaks'] = plotting.SimpleMultiplot(1, 1, xlabel="Gabor orientation (deg)", ylabel="Estimated orientation")
        panel = figures['peaks'].next_panel()
        panel.plot(real_peaks, peaks, 'ro')

